package kz.ruslovislo.bpmn.bpmnapi;

import kz.ruslovislo.bpmn.bpmnapi.model.myibatis.CustomMybatisMapper;
import kz.ruslovislo.bpmn.bpmnapi.security.SecurityFilter;
import org.flowable.engine.ProcessEngine;
import org.flowable.engine.ProcessEngineConfiguration;
import org.flowable.engine.impl.cfg.StandaloneProcessEngineConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Ruslan Temirbulatov on 1/5/20
 * @project bpmn-api
 */
public class BpmnApiComponent {



    public BpmnApiComponent(String jdbcUrl, String username, String password, String driver){

        System.out.println("spring.datasource.url = "+jdbcUrl);
        ProcessEngineConfiguration cfg = new StandaloneProcessEngineConfiguration()
                .setJdbcUrl(jdbcUrl)
                .setJdbcUsername(username)
                .setJdbcPassword(password)
                .setJdbcDriver(driver)
                .setDatabaseSchemaUpdate(ProcessEngineConfiguration.DB_SCHEMA_UPDATE_TRUE);

        setCustomMyIbatisMappers(cfg);
        ProcessEngine processEngine = cfg.buildProcessEngine();


    }

    private void setCustomMyIbatisMappers(ProcessEngineConfiguration cfg){
        Set<Class<?>> batisMappers = new HashSet<>();
        batisMappers.add(CustomMybatisMapper.class);
        cfg.setCustomMybatisMappers(batisMappers);
    }
}
