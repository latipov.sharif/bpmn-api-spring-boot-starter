package kz.ruslovislo.bpmn.bpmnapi;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author Ruslan Temirbulatov on 1/6/20
 * @project bpmn-api
 */
@ConfigurationProperties(prefix = "ruslovislo.bpmn.api")
public class BpmnApiProperties {

    private String jdbcUrl;
    private String username;
    private String password;
    private String driver;

    public String getJdbcUrl() {

        return jdbcUrl!=null?jdbcUrl:System.getProperty("spring.datasource.url");
    }

    public void setJdbcUrl(String jdbcUrl) {
        this.jdbcUrl = jdbcUrl;
    }

    public String getUsername() {
        return username!=null?username:System.getProperty("spring.datasource.username");
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password!=null?password:System.getProperty("spring.datasource.password");
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDriver() {
        return driver!=null?driver:System.getProperty("spring.datasource.driver-class-name");
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }
}
