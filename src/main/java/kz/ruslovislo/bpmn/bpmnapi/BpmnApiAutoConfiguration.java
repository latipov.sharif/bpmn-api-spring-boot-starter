package kz.ruslovislo.bpmn.bpmnapi;

import kz.ruslovislo.bpmn.bpmnapi.security.SecurityFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.WebApplicationContext;

/**
 * @author Ruslan Temirbulatov on 1/6/20
 * @project bpmn-api
 */
@Configuration
@ConditionalOnClass(BpmnApiComponent.class)
@EnableConfigurationProperties(BpmnApiProperties.class)
public class BpmnApiAutoConfiguration {


    @Autowired
    private BpmnApiProperties bpmnApiProperties;

    @Value("${spring.datasource.url}")
	private String jdbcUrl;

	@Value("${spring.datasource.username}")
	private String username;

	@Value("${spring.datasource.password}")
	private String password;

	@Value("${spring.datasource.driver-class-name}")
	private String driver;

    @Bean
    @ConditionalOnMissingBean
    public BpmnApiComponent bpmnApiComponent(){
        return new BpmnApiComponent(
                bpmnApiProperties.getJdbcUrl()!=null?bpmnApiProperties.getJdbcUrl():jdbcUrl,
                bpmnApiProperties.getUsername()!=null?bpmnApiProperties.getUsername():username,
                bpmnApiProperties.getPassword()!=null?bpmnApiProperties.getPassword():password,
                bpmnApiProperties.getDriver()!=null?bpmnApiProperties.getDriver():driver);
    }
}
