package kz.ruslovislo.bpmn.bpmnapi.model.pojo;

import java.io.Serializable;


import lombok.Data;
import org.flowable.engine.repository.ProcessDefinition;


@SuppressWarnings("serial")
@Data
public class AProcessDefinition implements Serializable{

	
	private String id;
	private String name;
	private String key;
	private String category;
	private String description;
	private String deploymentId;
	private String tenantId;
	private Integer version;
	
	public static AProcessDefinition parse(ProcessDefinition definition) {
		AProcessDefinition apd = new AProcessDefinition();
		apd.setId(definition.getId());
		apd.setKey(definition.getKey());
		apd.setName(definition.getName());
		apd.setCategory(definition.getCategory());
		apd.setDescription(definition.getDescription());
		apd.setDeploymentId(definition.getDeploymentId());
		apd.setTenantId(definition.getTenantId());
		
		apd.setVersion(definition.getVersion());
		return apd;
	}
}
