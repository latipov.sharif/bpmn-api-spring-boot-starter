package kz.ruslovislo.bpmn.bpmnapi.model.myibatis;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.Results;
public interface CustomMybatisMapper {

	@Results({
		@Result(property ="proc_inst_id_", column="proc_inst_id_"),
		@Result(property ="task_id_", column="task_id_"),
		@Result(property ="var_id_", column="var_id_"),
		@Result(property ="var_name_", column="var_name_"),
		@Result(property ="var_type_", column="var_type_"),
		@Result(property ="value_", column="value_"),
		@Result(property ="information_", column="information_")
	})
	
	@Select("SELECT ID_ FROM ACT_RE_PROCDEF WHERE KEY_ = #{key} limit 1")
	String loadProcessDefinitionIdByKey(String key);
	
	@Select("#{sql}")
	String sql(String sql);
	
	@Insert("insert into act2_task_varinst(proc_inst_id_, task_id_, var_id_, var_name_, var_type_, value_, information_) values(#{proc_inst_id_},#{task_id_},#{var_id_},#{var_name_},#{var_type_},#{value_},#{information_})")
	void insertNewTaskVariable(Village village);
	
	@Select("select * from act2_task_varinst where proc_inst_id_ = #{proc_inst_id_} and task_id_ = #{task_id_}")
	List<Village> selectTaskVariables(Village village);
	
	
	@Select("select count(*) from act2_task_varinst where proc_inst_id_ = #{proc_inst_id_} and task_id_ = #{task_id_} and var_id_ = #{var_id_}")
	Integer countTaskVariableExists(Village village);
	
//	@Update("insert into act2_task_varinst(proc_inst_id_, task_id_, var_id_, var_name_, var_type_) values(#{proc_inst_id_},#{task_id_},#{var_id_},#{var_name_},#{var_type_})")
//	void insertNewTaskVariable(Village village);
	
	
	

}
