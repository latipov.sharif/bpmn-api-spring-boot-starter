package kz.ruslovislo.bpmn.bpmnapi.model.pojo;

import java.io.Serializable;
import java.util.List;


import org.flowable.engine.ProcessEngines;
import org.flowable.engine.history.HistoricProcessInstance;
import org.flowable.engine.runtime.ProcessInstance;


import lombok.Data;

@SuppressWarnings("serial")
@Data
public class AStartedProcessInstance implements Serializable{

	private AProcessInstance process;
	
	private ATask task;	
	
	public static AStartedProcessInstance parse(ProcessInstance instance) {
		if(instance==null)
			return null;
		AStartedProcessInstance a = new AStartedProcessInstance();
		a.setProcess(AProcessInstance.parse(instance));
		a.setTask(ATask.findMyActualTask(instance));
		return a;
	}
}
