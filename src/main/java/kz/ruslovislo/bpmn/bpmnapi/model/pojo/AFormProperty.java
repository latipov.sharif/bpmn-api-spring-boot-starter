package kz.ruslovislo.bpmn.bpmnapi.model.pojo;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kz.ruslovislo.bpmn.bpmnapi.model.myibatis.Village;
import org.flowable.engine.ProcessEngines;
import org.flowable.engine.form.FormProperty;
import org.flowable.engine.form.FormType;
import org.flowable.engine.history.HistoricActivityInstance;
import org.flowable.engine.history.HistoricDetail;
import org.flowable.variable.api.history.HistoricVariableInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


import lombok.Data;

@SuppressWarnings("serial")
@Data

public class AFormProperty implements Serializable{

	private final static Logger log =LoggerFactory.getLogger(AFormProperty.class);
	
	private FormProperty property;
	private Map<String, Object> information=new HashMap<>(); 
	
	private final static ObjectMapper mapper = new ObjectMapper();
	
	
	public static AFormProperty parse(Village variable) {

		AFormProperty afp = new AFormProperty();
		FormProperty prop = new FormProperty() {
			
			@Override
			public boolean isWritable() {
				return false;
			}
			
			@Override
			public boolean isRequired() {
				return false;
			}
			
			@Override
			public boolean isReadable() {
				return true;
			}
			
			@Override
			public String getValue() {
				return (String)variable.getValue_();
			}
			
			@Override
			public FormType getType() {	
				if(variable.getVar_type_()==null)
					return null;
				return new FormType() {
					
					@Override
					public String getName() {
						return variable.getVar_type_();
					}
					
					@Override
					public Object getInformation(String key) {
						return null;
					}
				};
			}
			
			@Override
			public String getName() {
				return variable.getVar_name_();
			}
			
			@Override
			public String getId() {
				return variable.getVar_id_();
			}
		};
		
		afp.setProperty(prop);
		
		if(variable.getInformation_()!=null) {
			try {
				afp.setInformation(mapper.readValue(variable.getInformation_(), Map.class));
			} catch (Exception e) {
				e.printStackTrace();
				log.error("Error reading information_ "+variable.getInformation_(),e);
			}
			
		}

		return afp;
	}
	
	public static AFormProperty parse(FormProperty property) {
		AFormProperty afp = new AFormProperty();
		
		
		if(property.getType()!=null && property.getType().getName().equals("enum")) {
			afp.getInformation().put("values", property.getType().getInformation("values"));
			
		}
		if(property.getType()!=null && property.getType().getName().equals("date")) {
			afp.getInformation().put("datePattern", property.getType().getInformation("datePattern"));
			
		}
		if(property.getType()!=null && property.getValue()!=null && (property.getType().getName().equals("ftpfile") || property.getType().getName().equals("ftpimage") ) ) {
			
			String key = null;
			try {
				Map<String, Object> map = mapper.readValue(property.getValue(), Map.class);
				
				String server = (String)((Map)map.get("ftp")).get("server");
				Integer port = (Integer)((Map)map.get("ftp")).get("port");
				String user = (String)((Map)map.get("ftp")).get("user");
				String password = (String)((Map)map.get("ftp")).get("password");
				String path = (String)map.get("path");
				String filename = (String)map.get("filename");
//				key = BpmnSession.get().addFtpFileLink(server, port, user, password, path, filename);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			afp.getInformation().put("values", property.getType().getInformation("values"));
			afp.getInformation().put("download", key);
			
		}
		
		
		
		
		afp.setProperty(property);
	
		return afp;
	}
	
}

